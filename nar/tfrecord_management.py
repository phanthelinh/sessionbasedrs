import sys
import tensorflow as tf


def save_dataframe_to_tfrecord(df, make_sequence_example_fn, file_name):
    tf_options = tf.io.TFRecordOptions(compression_type='GZIP')
    tf_writer = tf.io.TFRecordWriter(file_name, tf_options)

    try:
        for index, row in df.iterrows():
            example = make_sequence_example_fn(row)
            tf_writer.write(example.SerializeToString())
    finally:
        tf_writer.close()
        sys.stdout.flush()
