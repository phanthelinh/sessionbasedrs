#!/bin/bash
DATA_DIR="/mnt/d/Documents/RecommenderSystems/00CODE/sessionbasedrs" && \
python3 -m nar.nar_trainer \
		--input_ace ${DATA_DIR}/data/pickles/article_content_embeddings.pickle \
		--input_data_train "${DATA_DIR}/data/pickles/data_train/data*.pickle" \
		--input_data_test "${DATA_DIR}/data/pickles/data_test/data*.pickle" \
		--model_path ${DATA_DIR}/data/saved_model/model_nar.h5 \
		--weight_path ${DATA_DIR}/data/saved_model/weight_nar.h5 \
		--rank_file_save "${DATA_DIR}/data/pickles/ranked_file_saved_*.pickle" \
		--input_publish_dates_dict ${DATA_DIR}/data/pickles/publish_date_article.pickle \
		--input_last_buffer ${DATA_DIR}/data/pickles/last_n_buffer.pickle \
		--input_true_pred "${DATA_DIR}/data/pickles/metric_true_hour/true_pred_*.pickle"