#!/bin/bash
DATA_DIR="/mnt/d/Documents/RecommenderSystems/00CODE/sessionbasedrs" && \
python3 -m nar.preprocessing.nar_preprocess \
		--input_ace ${DATA_DIR}/data/pickles/article_content_embeddings.pickle \
		--output_data_train "${DATA_DIR}/data/pickles/data_train/data*.pickle" \
		--input_data_click "${DATA_DIR}/data/clicks/session_hour=*" \
		--input_csv ${DATA_DIR}/data/genk_data/genk.csv \
		--output_dict_publish ${DATA_DIR}/data/pickles/publish_date_article.pickle