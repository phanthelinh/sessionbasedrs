import tensorflow as tf
import pickle
import numpy as np
import os
from keras.models import model_from_json
import pandas as pd
from datetime import datetime


def save_to_pickle(obj, filename):
    with tf.io.gfile.GFile(filename, 'wb') as handle:
        pickle.dump(obj, handle)


def parse_pickle_to_object(filename):
    with tf.io.gfile.GFile(filename, 'rb') as handle:
        return pickle.load(handle)


def merge_two_dicts(x, y):
    return {**x, **y}


def save_model_to_file(model_name, weight_name, model):
    model_json = model.to_json()
    with open(model_name, 'w') as json_file:
        json_file.write(model_json)
    # Serialize weights to HDF5
    model.save_weights(weight_name)


def load_model_from_file(model_name, weight_name):
    with open(model_name, 'r') as json_file:
        loaded_model_json = json_file.read()
    loaded_model = model_from_json(loaded_model_json)
    # Load weights
    loaded_model.load_weights(weight_name)
    return loaded_model


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def save_eval_benchmark_metrics_csv(eval_sessions_metrics_log, output_dir,
                                    training_hours_for_each_eval,
                                    output_csv='eval_stats_benchmarks.csv'):
    metrics_df = pd.DataFrame(eval_sessions_metrics_log)
    metrics_df = metrics_df.reset_index()
    metrics_df['hour'] = metrics_df['index'].apply(lambda x: ((x + 1) * training_hours_for_each_eval) % 24)
    metrics_df['day'] = metrics_df['index'].apply(lambda x: int(((x + 1) * training_hours_for_each_eval) / 24))

    csv_output_path = os.path.join(output_dir, output_csv)
    metrics_df.to_csv(csv_output_path, index=False)


def get_publish_date_dt(filepath):
    df = pd.read_csv(filepath, usecols=['newsId', 'publishDate'])
    df['publishDate'] = (df['publishDate'].apply(lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S').timestamp()))
    ids = df['newsId'].tolist()
    publish_dates = df['publishDate'].tolist()
    rs_dict = dict(zip(ids, publish_dates))
    return rs_dict
