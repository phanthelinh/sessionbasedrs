import os
import glob
import random
import numpy as np
from pathlib import Path
import argparse
import pandas as pd
import tensorflow as tf
import json
import keras.backend as K
from keras import regularizers
from keras.layers import Concatenate, Dense
import sys

from ..nar_common import MAX_DIM_USER_CONTEXT, STEP_DISPLAY, DIM_PREDICTED_ARTICLE, NUM_OF_NEG_SAMPLES
from ..util import parse_pickle_to_object, save_to_pickle, get_publish_date_dt


def create_args_parser():
    _parser = argparse.ArgumentParser()

    _parser.add_argument(
        '--input_ace', default='',
        help='Input Article Content Embeddings.')

    _parser.add_argument(
        '--output_data_train', default='',
        help='Output of data train.')

    _parser.add_argument(
        '--input_data_click', default='',
        help='Clicks folders.')

    _parser.add_argument(
        '--input_csv', default='')

    _parser.add_argument(
        '--output_dict_publish', default='')

    return _parser


parser = create_args_parser()
args = parser.parse_args()


def load_sessions_json_file(json_path):
    with open(json_path, 'r') as fi:
        for line in fi:
            yield json.loads(line)


def load_sessions_hour(session_hour_path):
    sessions = []
    session_files = sorted(glob.glob(session_hour_path))
    for f in session_files:
        sessions_hour = load_sessions_json_file(f)
        for session in sessions_hour:
            sessions.append(session)
    return sessions


def load_sessions_hours(folder_path):
    hour_folders = sorted(glob.glob(folder_path), key=lambda x: int(x.split('=')[1]))
    for hour_folder in hour_folders:
        hour_index = int(hour_folder.split('=')[1])
        # hour_folder_path = os.path.join(folder_path, hour_folder)
        hour_folder_path = hour_folder + '/*.json'
        sessions_hour = load_sessions_hour(hour_folder_path)
        yield (hour_index, sessions_hour)


def make_sequence_example_fn(row):
    context_features = {
        'user_id': tf.train.Feature(int64_list=tf.train.Int64List(value=[int(row['user_id'])])),
        'session_id': tf.train.Feature(int64_list=tf.train.Int64List(value=[row['session_id']])),
        'session_start': tf.train.Feature(float_list=tf.train.FloatList(value=[row['session_start']])),
        'session_size': tf.train.Feature(int64_list=tf.train.Int64List(value=[row['session_size']])),
        'click_os': tf.train.Feature(int64_list=tf.train.Int64List(value=[row['click_os']])),
        'loc_id': tf.train.Feature(int64_list=tf.train.Int64List(value=[row['loc_id']]))
    }
    context = tf.train.Features(feature=context_features)

    sequence_features = {
        'click_timestamp': tf.train.FeatureList(feature=[
            tf.train.Feature(float_list=tf.train.FloatList(value=[val])) for val in row['click_timestamp']]),
        'click_article_id': tf.train.FeatureList(feature=[
            tf.train.Feature(int64_list=tf.train.Int64List(value=[val])) for val in row['click_article_id']])
    }
    sequence_features_list = tf.train.FeatureLists(feature_list=sequence_features)

    return tf.train.SequenceExample(context=context, feature_lists=sequence_features_list)


def process_sessions_hour(session_hour):
    rs = []
    for sess in session_hour:
        new_sess = {
            'session_id': sess['session_id'],
            'session_size': sess['session_size'],
            'session_start': sess['session_start'],
            'user_id': sess['user_id']
        }
        articles, ts = [], [],
        for click in sess['clicks']:
            articles.append(click['click_article_id'])
            os = click['click_os']
            ts.append(click['click_timestamp'])
            loc = click['loc_id']
        new_sess['click_article_id'] = articles
        new_sess['click_os'] = os
        new_sess['click_timestamp'] = ts
        new_sess['loc_id'] = loc
        rs.append(new_sess)
    return pd.DataFrame(rs)


def encode_for_loc_id(value):
    rs = np.zeros(shape=(MAX_DIM_USER_CONTEXT,), dtype=np.int)
    value = MAX_DIM_USER_CONTEXT - 2 if value > MAX_DIM_USER_CONTEXT - 2 else value
    rs[value + 1] = 1
    return rs


def encode_for_os_click(value):
    rs = np.zeros(shape=(MAX_DIM_USER_CONTEXT,), dtype=np.int)
    rs[value - 1] = 1
    return rs


def encode_for_timestamp(value):
    value = str(int(value))
    loop_val = []
    loop_val.extend(str(value))
    rs = np.zeros(shape=(MAX_DIM_USER_CONTEXT,), dtype=np.int)
    for i, v in enumerate(loop_val):
        rs[i] = v
    return rs


def get_publish_ace_list(news_ids, ace_list, key=0, publish_list=None):
    rs = []
    for i in news_ids:
        if key == 1:
            val = ace_list.get(i)
            if val is not None:
                rs.append(val[0])
            else:
                continue
                # rs.append(np.zeros(shape=(1, MAX_DIM_USER_CONTEXT)).tolist())
        else:
            val = publish_list.get(i)
            if val is not None:
                rs.append(encode_for_timestamp(val))
            else:
                continue
    return rs


def process_xy_train_sets(x_set, y_set):
    x_train, y_train, neg_train = [], [], []
    total = len(x_set)
    for i, t_data in enumerate(x_set):
        sys.stdout.write('\r----------> Processing train set at %d/%d' % (i, total))
        sys.stdout.flush()
        seq = x_set[i]
        seq_train, neg_seq_train = [], []
        with tf.compat.v1.variable_scope('user_personalized_embedding'):
            for j, article in enumerate(seq[0]):
                v1 = tf.constant(np.array(article).reshape(1, MAX_DIM_USER_CONTEXT), dtype=tf.float32)
                v2 = tf.constant(np.array(seq[1][j]).reshape(1, MAX_DIM_USER_CONTEXT), dtype=tf.float32)
                v3 = tf.constant(np.array(seq[2][j]).reshape(1, MAX_DIM_USER_CONTEXT), dtype=tf.float32)
                v4 = tf.constant(np.array(seq[3]).reshape(1, MAX_DIM_USER_CONTEXT), dtype=tf.float32)
                v5 = tf.constant(np.array(seq[4]).reshape(1, MAX_DIM_USER_CONTEXT), dtype=tf.float32)
                concat_attr = Concatenate()([v1, v2, v3, v4, v5])
                concat_attr = Dense(units=DIM_PREDICTED_ARTICLE, activation='relu', kernel_regularizer=regularizers.l2,
                                    name='user_personalized_embedding')(concat_attr)
                concat_attr = K.eval(concat_attr).ravel()
                seq_train.append(concat_attr)

        with tf.compat.v1.variable_scope('calc_label'):
            concat_labels = Concatenate() \
                ([tf.constant(np.array(label).reshape(1, MAX_DIM_USER_CONTEXT), dtype=tf.float32)
                  for label in y_set[i]])
            concat_labels = Dense(units=DIM_PREDICTED_ARTICLE, activation='relu')(concat_labels)
            concat_labels = K.eval(concat_labels)
        x_train.append(seq_train)
        y_train.append(concat_labels)

    with tf.compat.v1.variable_scope('create_neg_samples'):
        for j in range(len(y_train)):
            sets = y_train[:j] + y_train[j + 1:]
            temp = random.sample(sets, k=NUM_OF_NEG_SAMPLES)
            seq_neg_train = [np.array(t.tolist()[0]) for t in temp]
            seq_neg_train.append(np.array(y_train[j].tolist()[0]))
            neg_train.append(seq_neg_train)
    return x_train, y_train, neg_train


def create_training_set(dataset, ace, publish_date):
    sys.stdout.write('\rCreating training set...')
    # sys.stdout.flush()
    train_set, labels_set = [], []
    for i, data in dataset.iterrows():
        ace_list = get_publish_ace_list(data['click_article_id'], ace, 1, publish_date)
        publish_date_list = get_publish_ace_list(data['click_article_id'], ace, 0, publish_date)

        if len(ace_list) < 2 or len(ace_list) > 20:
            continue
        list_timestamp = data['click_timestamp']
        os = data['click_os']
        location = data['loc_id']
        ts_list = [encode_for_timestamp(ts) for ts in list_timestamp]
        os = encode_for_os_click(os)
        loc = encode_for_loc_id(location)
        session_seq = [ace_list[:-1], publish_date_list[:-1], ts_list[:-1], os, loc]
        train_set.append(session_seq)
        labels_set.append([ace_list[-1], publish_date_list[-1], ts_list[-1], os, loc])
    sys.stdout.write('\rCreated %d train samples.\n' % len(train_set))
    sys.stdout.flush()
    train_set, labels_set, neg_set = process_xy_train_sets(train_set, labels_set)
    return train_set, labels_set, neg_set


def main():
    # dir_name = os.path.dirname(__file__)
    # root_path = Path(Path(dir_name).parent).parent
    # input_csv = os.path.join(root_path, 'data/genk_data/genk.csv')
    input_csv = args.input_csv
    # output_dict_publish = os.path.join(root_path, 'data/pickles/publish_date_article.pickle')
    output_dict_publish = args.output_dict_publish
    dict_publish = get_publish_date_dt(input_csv)
    save_to_pickle(dict_publish, output_dict_publish)

    # input_ace = os.path.join(root_path, 'data/pickles/article_content_embeddings.pickle')
    input_ace = args.input_ace
    print('Loading Article Content Embedding from {}'.format(input_ace))
    ace = parse_pickle_to_object(input_ace)

    count_processed_data = 0

    # output_data_train = os.path.join(root_path, 'data/pickles/data_train/data*.pickle')
    output_data_train = args.output_data_train
    print('Create and process data train according to session hour...')
    # input_data_click = os.path.join(root_path, 'data/clicks/session_hour=*')
    input_data_click = args.input_data_click
    for (hour_index, sessions_hour) in load_sessions_hours(input_data_click):
        if hour_index % 10 == 0:
            print('Processing hour index %d' % hour_index)
        sessions_converted = process_sessions_hour(sessions_hour)
        with tf.compat.v1.variable_scope('create_sets'):
            x_train, y_train, neg_train = create_training_set(sessions_converted, ace, dict_publish)
            count_processed_data = len(y_train)
            sys.stdout.write('\rProcessed %d items.' % count_processed_data)
            sys.stdout.flush()
            if count_processed_data % STEP_DISPLAY == 0:
                print('\rProcessed %d items.' % count_processed_data)
            if len(y_train) > 0:
                save_to_pickle((x_train, y_train, neg_train),
                               output_data_train.replace('*', '{0:03d}').format(hour_index))
    print('Done processing data with {} items. Save to {}'.format(count_processed_data, output_data_train))


if __name__ == '__main__':
    main()
