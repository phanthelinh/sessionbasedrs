import glob
import argparse
import json
import pandas as pd
import tensorflow as tf
from nar.util import save_to_pickle, parse_pickle_to_object
from keras.layers import Concatenate, Dense
import keras.backend as K
from nar.nar_common import DIM_PREDICTED_ARTICLE, MAX_DIM_USER_CONTEXT
from keras import regularizers

import numpy as np


def create_args_parser():
    _parser = argparse.ArgumentParser()

    _parser.add_argument(
        '--input_data_click', default='',
        help='Clicks folders.')

    _parser.add_argument(
        '--input_csv', default='')

    _parser.add_argument(
        '--input_dict_publish', default='')
    return _parser


parser = create_args_parser()
args = parser.parse_args()


def load_sessions_json_file(json_path):
    with open(json_path, 'r') as fi:
        for line in fi:
            yield json.loads(line)


def load_sessions_hour(session_hour_path):
    sessions = []
    session_files = sorted(glob.glob(session_hour_path))
    for f in session_files:
        sessions_hour = load_sessions_json_file(f)
        for session in sessions_hour:
            sessions.append(session)
    return sessions


def load_sessions_hours(folder_path):
    hour_folders = sorted(glob.glob(folder_path), key=lambda x: int(x.split('=')[1]))
    for hour_folder in hour_folders:
        hour_index = int(hour_folder.split('=')[1])
        hour_folder_path = hour_folder + '/*.json'
        sessions_hour = load_sessions_hour(hour_folder_path)
        yield (hour_index, sessions_hour)


def process_sessions_hour(session_hour):
    rs = []
    for sess in session_hour:
        new_sess = {
            'session_id': sess['session_id'],
            'session_size': sess['session_size'],
            'session_start': sess['session_start'],
            'user_id': sess['user_id']
        }
        articles, ts = [], [],
        for click in sess['clicks']:
            articles.append(click['click_article_id'])
            os = click['click_os']
            ts.append(click['click_timestamp'])
            loc = click['loc_id']
        new_sess['click_article_id'] = articles
        new_sess['click_os'] = os
        new_sess['click_timestamp'] = ts
        new_sess['loc_id'] = loc
        rs.append(new_sess)
    return rs


def calc_diff_ts(ts, publishDate):
    pub = publishDate.get(ts[0])
    if pub is not None:
        return int(ts[1]) - int(pub)
    else:
        return int(ts[1])


def encode_for_timestamp(value):
    value = str(int(value))
    loop_val = []
    loop_val.extend(str(value))
    rs = np.zeros(shape=(MAX_DIM_USER_CONTEXT,), dtype=np.int)
    for i, v in enumerate(loop_val):
        rs[i] = v
    return rs


def main():
    # input_data_click = args.input_data_click
    input_data_click = r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\clicks\*'
    sess_hours = load_sessions_hours(input_data_click)
    sesses = []
    for (idx, sess_hour) in sess_hours:
        sess = process_sessions_hour(sess_hour)
        for s in sess:
            df2 = pd.DataFrame.from_dict(s)
            sesses.append(df2)
    df = sesses[-1]
    for s in sesses[:-1]:
        df = df.append(s)
    print(df.head())
    df.to_csv(r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\buffer.csv')

    d_f = pd.read_csv(r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\buffer.csv')

    # Calculate recent popularity
    d_count_article = d_f.groupby('click_article_id').count().sort_values(ascending=False, by='Unnamed: 0').reset_index()
    d_count_article.to_csv(r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\count.csv')
    # Calculate recency
    l = list(d_count_article['click_article_id'].head(100))
    d_ts = d_f[d_f['click_article_id'].isin(l)]
    d_ts = d_ts.drop(columns=['Unnamed: 0', 'session_id', 'session_size', 'session_start', 'user_id', 'click_os', 'loc_id'])
    pblisDate = parse_pickle_to_object(r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\publish_date_article.pickle')
    d_ts['diff_ts'] = (d_ts.apply(lambda x: calc_diff_ts(x, pblisDate), axis=1))
    d_ts = d_ts.sort_values(by='diff_ts')
    l2 = list(d_ts['click_article_id'].head(1000))
    # save_to_pickle(l2, r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\last_n_buffer.pickle')
    d_ts.to_csv(r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\ts.csv')

    # Get ACE
    input_ace = r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\article_content_embeddings.pickle'
    print('Loading Article Content Embedding from {}'.format(input_ace))
    ace = parse_pickle_to_object(input_ace)
    save_obj = {}
    for news_id in l2:
        _ace = ace.get(news_id)
        if _ace is None:
            continue
        compare_ace = tf.constant(np.array(_ace[0]).reshape(1, -1), dtype=tf.float32)
        compare_ts = tf.constant(np.array(encode_for_timestamp(pblisDate.get(news_id))).reshape(1, -1),
                                 dtype=tf.float32)
        # Location, device
        with tf.compat.v1.variable_scope('user_personalized'):
            concat = Concatenate()([compare_ace, compare_ts])
            compare_vec = Dense(units=DIM_PREDICTED_ARTICLE, activation='relu',
                                kernel_regularizer=regularizers.l2)(concat)
            compare_vec = K.eval(compare_vec).ravel()
            compare_vec = np.array(compare_vec).reshape(1, -1)
            save_obj[news_id] = compare_vec
    save_to_pickle(save_obj, r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\last_n_buffer.pickle')


if __name__ == '__main__':
    main()