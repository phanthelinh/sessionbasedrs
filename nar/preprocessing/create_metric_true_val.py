import glob
import json
import pandas as pd
from nar.util import save_to_pickle, parse_pickle_to_object
import numpy as np
from nar.nar_common import MAX_DIM_USER_CONTEXT


def load_sessions_json_file(json_path):
    with open(json_path, 'r') as fi:
        for line in fi:
            yield json.loads(line)


def load_sessions_hour(session_hour_path):
    sessions = []
    session_files = sorted(glob.glob(session_hour_path))
    for f in session_files:
        sessions_hour = load_sessions_json_file(f)
        for session in sessions_hour:
            sessions.append(session)
    return sessions


def load_sessions_hours(folder_path):
    hour_folders = sorted(glob.glob(folder_path), key=lambda x: int(x.split('=')[1]))
    for hour_folder in hour_folders:
        hour_index = int(hour_folder.split('=')[1])
        hour_folder_path = hour_folder + '/*.json'
        sessions_hour = load_sessions_hour(hour_folder_path)
        yield (hour_index, sessions_hour)


def process_sessions_hour(session_hour):
    rs = []
    for sess in session_hour:
        new_sess = {
            'session_id': sess['session_id'],
            'session_size': sess['session_size'],
            'session_start': sess['session_start'],
            'user_id': sess['user_id']
        }
        articles, ts = [], [],
        for click in sess['clicks']:
            articles.append(click['click_article_id'])
            os = click['click_os']
            ts.append(click['click_timestamp'])
            loc = click['loc_id']
        new_sess['click_article_id'] = articles
        new_sess['click_os'] = os
        new_sess['click_timestamp'] = ts
        new_sess['loc_id'] = loc
        rs.append(new_sess)
    return pd.DataFrame(rs)


def create_training_set(dataset, ace, publish_date):
    labels_set = []
    # for i, data in dataset.iterrows():
    #     ace_list = get_publish_ace_list(data['click_article_id'], ace, 1, publish_date)
    #     if len(ace_list) < 2:
    #         continue
    #     labels_set.append(data['click_article_id'][-1])
    #     print(len(labels_set))
    train_set, labels_set = [], []
    for i, data in dataset.iterrows():
        ace_list = get_publish_ace_list(data['click_article_id'], ace, 1, publish_date)
        publish_date_list = get_publish_ace_list(data['click_article_id'], ace, 0, publish_date)

        if len(ace_list) < 2:
            continue
        labels_set.append(data['click_article_id'][-1])
    return labels_set


def encode_for_timestamp(value):
    value = str(int(value))
    loop_val = []
    loop_val.extend(str(value))
    rs = np.zeros(shape=(MAX_DIM_USER_CONTEXT,), dtype=np.int)
    for i, v in enumerate(loop_val):
        rs[i] = v
    return rs


def get_publish_ace_list(news_ids, ace_list, key=0, publish_list=None):
    rs = []
    for i in news_ids:
        if key == 1:
            val = ace_list.get(i)
            if val is not None:
                rs.append(val[0])
            else:
                continue
                # rs.append(np.zeros(shape=(1, MAX_DIM_USER_CONTEXT)).tolist())
        else:
            val = publish_list.get(i)
            if val is not None:
                rs.append(encode_for_timestamp(val))
            else:
                continue
    return rs


def main():
    input_data_click = r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\hour\*'
    sess_hours = load_sessions_hours(input_data_click)
    pblisDate = parse_pickle_to_object(r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\publish_date_article.pickle')
    input_ace = r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\article_content_embeddings.pickle'
    ace = parse_pickle_to_object(input_ace)
    labels = []
    for (idx, sess_hour) in sess_hours:
        sess = process_sessions_hour(sess_hour)
        labels += create_training_set(sess, ace, pblisDate)
    save_to_pickle(labels, r'D:\Documents\RecommenderSystems\00CODE\sessionbasedrs\data\pickles\metric_true_hour\true_pred_*.pickle'.replace('*', str(len(labels))))


if __name__ == '__main__':
    main()
