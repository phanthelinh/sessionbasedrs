import numpy as np


def calc_MRR(y_true, ranked_res):
    rr = []
    for idx, art in enumerate(y_true):
        l_cosine = ranked_res[idx]
        if art in l_cosine:
            r = l_cosine.index(art)
            rr.append(1/(r+1))
        else:
            rr.append(0)
    return np.mean(rr)


def calc_accuracy(y_true, ranked_res):
    acc = []
    for idx, art in enumerate(y_true):
        l_cosine = ranked_res[idx]
        if art in l_cosine:
            acc.append(1)
        else:
            acc.append(0)
    return np.mean(acc)


def calc_precision_recall(y_true, ranked_res):
    tp = 0
    fp = 0
    fn = 0
    for idx, art in enumerate(y_true):
        l_ranked = ranked_res[idx]
        middle_pos = (len(l_ranked)/2) - 1
        if art in l_ranked:
            pos = l_ranked.index(art)
            if pos <= middle_pos:
                tp += 1
            else:
                fn += 1
        else:
            fp += 1
    prec = tp/(tp + fp)
    recall = tp/(tp + fn)
    return prec, recall