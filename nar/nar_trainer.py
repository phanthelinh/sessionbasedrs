import tensorflow as tf
import logging
import os
import glob
from pathlib import Path
import argparse
from datetime import datetime

from .util import parse_pickle_to_object
from .nar_model import NAR_Model


tfv1 = tf.compat.v1
tfv1.logging.set_verbosity(tf.compat.v1.logging.INFO)


def create_args_parser():
    _parser = argparse.ArgumentParser()

    _parser.add_argument(
            '--input_ace', default='',
            help='Input Article Content Embeddings.')

    _parser.add_argument(
            '--input_data_train', default='')

    _parser.add_argument(
        '--input_data_test', default='')

    _parser.add_argument(
        '--model_path', default='')

    _parser.add_argument(
        '--weight_path', default='')

    _parser.add_argument(
        '--rank_file_save', default='')

    _parser.add_argument(
        '--input_publish_dates_dict', default='')

    _parser.add_argument(
        '--input_last_buffer', default='')

    _parser.add_argument(
        '--input_true_pred', default='')

    return _parser


parser = create_args_parser()
args = parser.parse_args()


def convert_datetime_timstamp(value):
    date_obj = datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
    return int(datetime.timestamp(date_obj))


def main(unused_argv):
    # dir_name = os.path.dirname(__file__)
    # root_path = Path(Path(dir_name).parent)
    logging.getLogger('tensorflow').propagate = False
    try:
        input_ace = args.input_ace
        # input_ace = os.path.join(root_path, 'data/pickles/article_content_embeddings.pickle')
        print('Loading Article Content Embedding from {}'.format(input_ace))
        ace = parse_pickle_to_object(input_ace)

        # input_data_train = os.path.join(root_path, 'data/pickles/data_train/data*')
        input_data_train = args.input_data_train
        # input_data_test = os.path.join(root_path, 'data/pickles/data_test/data*')
        input_data_test = args.input_data_test
        data_file_names = sorted(glob.glob(input_data_train))
        x_train, y_train, neg_train = [], [], []
        # for file in data_file_names:
        #     x, y, neg = parse_pickle_to_object(file)
        #     x_train += x
        #     y_train += y
        #     neg_train += neg
        print('Total data train loaded: %d' % len(y_train))
        data_file_names = sorted(glob.glob(input_data_test))
        x_test, y_test, neg_test = [], [], []
        for file in data_file_names:
            x, y, neg = parse_pickle_to_object(file)
            x_test += x
            y_test += y
            neg_test += neg
        print('Total data test loaded: %d' % len(y_test))
        rank_file_save = args.rank_file_save
        input_publish_dates_dict = args.input_publish_dates_dict
        publish_dates = parse_pickle_to_object(input_publish_dates_dict)
        input_last_buffer = args.input_last_buffer
        last_buffer = parse_pickle_to_object(input_last_buffer)
        input_true_pred = args.input_true_pred.replace('*', str(len(y_test)))
        true_preds = parse_pickle_to_object(input_true_pred)

        params = {
            'x_train': x_train,
            'y_train': y_train,
            'neg_train': neg_train,
            'model_path': args.model_path,
            'weight_path': args.weight_path,
            'x_test': x_test,
            'y_test': y_test,
            'neg_test': neg_test,
            'ace': ace,
            'top_k': 20,
            'rank_file_save': rank_file_save,
            'publish_dates': publish_dates,
            'last_buffer': last_buffer,
            'true_preds': true_preds
        }
        nar_model = NAR_Model(params)
        # nar_model.train_model()
        nar_model.predict_model()
    except Exception as ex:
        tfv1.logging.error('ERROR: {}'.format(ex))
        raise


if __name__ == '__main__':
    tfv1.app.run()
