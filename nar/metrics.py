from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np


class StreamingMetric:
    name = 'undefined'

    def __init__(self, topn):
        self.topn = topn
        self.reset()

    def reset(self):
        pass

    def add(self, predictions, labels):
        pass

    def result(self):
        pass


class MRR(StreamingMetric):
    name = 'mrr_at_n'

    def __init__(self, topn):
        super().__init__(topn)

    def reset(self):
        self.mrr_results = []

    def add(self, predictions, labels):
        measures = []
        for row_idx, session_labels in enumerate(labels):
            for col_idx, item_label in enumerate(session_labels):
                if item_label != 0:
                    correct_preds = (item_label == predictions[row_idx, col_idx])[:self.topn].astype(np.int32)
                    correct_preds_pos = np.where(correct_preds)[0]

                    reciprocal_rank = 0
                    if len(correct_preds_pos) > 0:
                        reciprocal_rank = 1.0 / (1 + correct_preds_pos[0])
                    measures.append(reciprocal_rank)
        self.mrr_results.extend(measures)

    def result(self):
        avg_mrr = np.mean(self.mrr_results)
        return avg_mrr

