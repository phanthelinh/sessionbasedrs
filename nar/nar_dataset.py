import tensorflow as tf
from nar.util import merge_two_dicts


def parse_sequence_example(example):
    context_features = {
        'user_id': tf.io.FixedLenFeature([], dtype=tf.int64),
        'session_id': tf.io.FixedLenFeature([], dtype=tf.int64),
        'session_start': tf.io.FixedLenFeature([], dtype=tf.float32),
        'session_size': tf.io.FixedLenFeature([], dtype=tf.int64),
        'click_os': tf.io.FixedLenFeature([], dtype=tf.int64),
        'loc_id': tf.io.FixedLenFeature([], dtype=tf.int64)
    }

    sequence_features = {
        'click_timestamp': tf.io.FixedLenSequenceFeature(shape=[], dtype=tf.float32),
        'click_article_id': tf.io.FixedLenSequenceFeature(shape=[], dtype=tf.int64)
    }

    context_example, sequence_example = tf.io.parse_single_sequence_example(example,
                                                                            context_features=context_features,
                                                                            sequence_features=sequence_features)
    merged_example = merge_two_dicts(context_example, sequence_example)
    return merged_example


def get_data_set(file_names):
    ds = tf.data.TFRecordDataset(file_names, compression_type='GZIP')
    ds = ds.map(lambda x: parse_sequence_example(x))
    return ds.take(-1)
