import numpy as np
import tensorflow as tf
import keras
from keras.layers import Dense, LSTM, Input, Lambda, Concatenate
from keras.models import Model
from keras import regularizers
from keras import optimizers
import keras.backend as K
from tqdm import tqdm
import sys
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter
import matplotlib.pyplot as plt
from .evaluation import calc_MRR, calc_accuracy, calc_precision_recall

from .nar_common import DIM_PREDICTED_ARTICLE, BATCH_SIZE, EPOCHS, MAX_DIM_USER_CONTEXT
from .util import save_model_to_file, load_model_from_file, save_to_pickle, parse_pickle_to_object


def norm_value(val):
    return K.sqrt(K.sum(K.square(val), axis=-1))


def cosine_sim(a, b, is_transpose=False):
    ts1 = K.dot(a, K.transpose(b))
    ts2 = (norm_value(a) * norm_value(b)) if not is_transpose else K.transpose((norm_value(a) * norm_value(b)))
    return Lambda(lambda x: x[0] / x[1])([ts1, ts2])


def softmax_pos_item(y_true, y_pred, neg_train):
    x = cosine_sim(y_true, y_pred)
    x = K.exp(x)
    y = cosine_sim(neg_train, y_pred, True)
    y = K.exp(y)
    y = K.sum(y)
    return x / y


def loss_fn(y_true, y_pred, neg_train):
    return -K.log(K.prod(softmax_pos_item(y_true, y_pred, neg_train)))


def loss_function(neg):
    def loss(y_true, y_pred):
        return loss_fn(y_true, y_pred, neg)

    return loss


def encode_for_timestamp(value):
    value = str(int(value))
    loop_val = []
    loop_val.extend(str(value))
    rs = np.zeros(shape=(MAX_DIM_USER_CONTEXT,), dtype=np.int)
    for i, v in enumerate(loop_val):
        rs[i] = v
    return rs


class NAR_Model:
    def __init__(self, params):
        self.model_path = params['model_path']
        self.weight_path = params['weight_path']
        self.x_train = params['x_train']
        self.y_train = params['y_train']
        self.neg_train = params['neg_train']
        self.x_test = params['x_test']
        self.y_test = params['y_test']
        self.neg_test = params['neg_test']
        self.ace = params['ace']
        self.top_k = params['top_k']
        self.rank_file_save = params['rank_file_save']
        self.publish_dates = params['publish_dates']
        self.last_buffer = params['last_buffer']
        self.true_preds = params['true_preds']

    def build_lstm(self):
        print('Building model...')
        input_lstm = Input(shape=(None, DIM_PREDICTED_ARTICLE))
        lstm = LSTM(units=255, kernel_regularizer=regularizers.l2(1e-4))(input_lstm)
        lstm = Dense(units=512, name='session_representation')(lstm)
        lstm = Dense(units=DIM_PREDICTED_ARTICLE, name='predicted_article_embedding')(lstm)
        model = Model(inputs=input_lstm, outputs=lstm)

        y_true = Input(shape=[DIM_PREDICTED_ARTICLE])
        y_pred = model(input_lstm)
        neg_train = Input(shape=(None, DIM_PREDICTED_ARTICLE))
        loss_value = loss_fn(y_true, y_pred, neg_train)

        optimizer = optimizers.Adam(1e-3)
        update_op = optimizer.get_updates(loss=loss_value, params=model.trainable_weights)

        train = K.function(inputs=[input_lstm, neg_train, y_true], outputs=loss_value, updates=update_op)
        test = K.function(inputs=[input_lstm, neg_train, y_true], outputs=loss_value)
        # Training loop
        print('Starting training loop.')
        for epoch in range(EPOCHS):
            print('\n\nEpoch %d:' % epoch)

            pbar = tqdm(range(len(self.x_train)))
            losses_train = []
            for i in pbar:
                sample = [self.x_train[i]]
                target = self.y_train[i]
                neg_sample = self.neg_train[i]

                sample = np.array(sample)
                target = np.array(target)
                neg_sample = np.array(neg_sample)
                neg_sample = tf.expand_dims(neg_sample, 0)

                # Running train graph
                loss_train = train([sample, neg_sample, target])
                losses_train.append(loss_train)
                loss_train_mean = np.mean(losses_train)

                pbar.set_description('Train loss: %.3f' % loss_train_mean)

            # Testing
            losses_test = []
            for j in range(len(self.y_test)):
                sample_test = np.array([self.x_test[j]])
                target_test = np.array(self.y_test[j])
                neg_sample_test = np.array([self.neg_test[j]])

                loss_test = test([sample_test, neg_sample_test, target_test])
                losses_test.append(loss_test)
            loss_test_mean = np.mean(losses_test)
            print('Loss test mean %.3f' % loss_test_mean)

        print('Saving model to: {} and weights to: {}'.format(self.model_path, self.weight_path))
        save_model_to_file(self.model_path, self.weight_path, model)

    def test_model(self):
        print('Start testing model.\nReloading model...')
        neg_train = Input(shape=(None, DIM_PREDICTED_ARTICLE))
        model = keras.models.load_model(self.model_path, custom_objects={'loss': loss_function(neg_train),
                                                                         'AdditionalInput': AdditionalInput})
        optimizer = optimizers.Adam(1e-3)
        model.summary()
        y = model.input_names
        model.compile(optimizer=optimizer, loss=loss_function(model.get_layer('negative_sample').input), metrics=['accuracy'])

        def generate_data():
            while True:
                for i in range(len(self.y_test)):
                    x = np.array([self.x_test[i]])
                    y = np.array(self.y_test[i])
                    n = np.array([self.neg_test[i]])
                    yield ({'input_lstm': x, 'negative_sample': n, 'y_true': y}, {'predicted_article_embedding': y})

        loss, acc = model.evaluate_generator(generate_data(), steps=len(self.y_train), verbose=2)
        u = model.metrics_names
        # print(history.history.keys())
        # plt.plot(history.history['accuracy'])
        # plt.title('Session-based model accuracy')
        # plt.ylabel('accuracy')
        # plt.xlabel('epoch')
        # plt.legend(['test'], loc='upper left')
        # plt.show()

    def train_model(self):
        print('Building model...')
        y_true = Input(shape=[DIM_PREDICTED_ARTICLE], name='y_true')
        neg_train = Input(shape=(None, DIM_PREDICTED_ARTICLE), name='negative_sample')
        input_lstm = Input(shape=(None, DIM_PREDICTED_ARTICLE), name='input_lstm')
        # extra_input = AdditionalInput()([input_lstm, neg_train, y_true])
        lstm = LSTM(units=255, kernel_regularizer=regularizers.l2(1e-4))(input_lstm)
        lstm = Dense(units=512, name='session_representation')(lstm)
        lstm = Dense(units=DIM_PREDICTED_ARTICLE, name='predicted_article_embedding')(lstm)

        model = Model(inputs=[input_lstm, neg_train, y_true], outputs=lstm)
        model.compile(loss=loss_function(neg_train), optimizer='adam', metrics=['accuracy'])

        def generate_data_train():
            while True:
                for i in range(len(self.y_train)):
                    x = np.array([self.x_train[i]])
                    y = np.array(self.y_train[i])
                    n = np.array([self.neg_train[i]])
                    yield ({'input_lstm': x, 'negative_sample': n, 'y_true': y}, {'predicted_article_embedding': y})

        def generate_data_test():
            while True:
                for i in range(len(self.y_test)):
                    x = np.array([self.x_test[i]])
                    y = np.array(self.y_test[i])
                    n = np.array([self.neg_test[i]])
                    yield ({'input_lstm': x, 'negative_sample': n, 'y_true': y}, {'predicted_article_embedding': y})

        print('Training....')
        model.fit_generator(generate_data_train(), steps_per_epoch=len(self.y_train), epochs=EPOCHS, verbose=2)
        print('Evaluating...')
        loss, acc = model.evaluate_generator(generate_data_test(), steps=len(self.y_test))
        print('Loss %.3f, accuracy %.3f' % (loss, acc))

        print('Saving model to: {} and weights to: {}'.format(self.model_path, self.weight_path))
        model.save(self.model_path)

    def predict_model(self):
        print('Start testing model.\nReloading model...')
        neg_train = Input(shape=(None, DIM_PREDICTED_ARTICLE))
        model = keras.models.load_model(self.model_path, custom_objects={'loss': loss_function(neg_train),
                                                                         'AdditionalInput': AdditionalInput})

        def generate_data_predict():
            while True:
                for i in range(len(self.y_test)):
                    x = np.array([self.x_test[i]])
                    y = np.array(self.y_test[i])
                    yield ({'input_lstm': x}, {'predicted_article_embedding': y})
        print('Predicting...')
        preds = model.predict_generator(generate_data_predict(), steps=len(self.y_test))
        total_preds = len(preds)
        # Ranking
        rank_results = []
        for idx, pred in enumerate(preds):
            expected = pred.reshape(1, -1)  # for calc similarity
            cos_dict = {}
            sys.stdout.write('\rCalculating predictions for prediction %d/%d' % (idx, total_preds))
            sys.stdout.flush()
            for news_id, compare_vec in self.last_buffer.items():
                # ace = self.ace.get(news_id)
                # if ace is None:
                #     continue
                # compare_ace = tf.constant(np.array(ace[0]).reshape(1, -1), dtype=tf.float32)
                # compare_ts = tf.constant(np.array(encode_for_timestamp(self.publish_dates.get(news_id))).reshape(1, -1),
                #                          dtype=tf.float32)
                # # Location, device
                # with tf.compat.v1.variable_scope('user_personalized'):
                #     concat = Concatenate()([compare_ace, compare_ts])
                #     compare_vec = Dense(units=DIM_PREDICTED_ARTICLE, activation='relu',
                #                         kernel_regularizer=regularizers.l2)(concat)
                #     compare_vec = K.eval(compare_vec).ravel()
                #     compare_vec = np.array(compare_vec).reshape(1, -1)
                #     cos_val = cosine_similarity(expected, compare_vec).tolist()[0][0]
                #     cos_dict[news_id] = cos_val
                cos_val = cosine_similarity(expected, compare_vec).tolist()[0][0]
                cos_dict[news_id] = cos_val
            # Rank list
            # k = Counter(cos_dict)
            # top_cos = k.most_common(self.top_k)
            # ranked_list = [ranked_news_id[0] for ranked_news_id in top_cos]
            sort_list = sorted(cos_dict, key=cos_dict.get, reverse=True)
            top_cos = sort_list[:self.top_k]
            ranked_list = [ranked_news_id for ranked_news_id in top_cos]
            rank_results.append(ranked_list)
        filepath = self.rank_file_save.replace('*', '{0:d}'.format(total_preds))
        print('Saving predicted results to %s' % filepath)
        save_to_pickle(rank_results, filepath)
        print('MRR metrics: %f' % calc_MRR(self.true_preds, rank_results))
        print('ACC metrics: %f' % calc_accuracy(self.true_preds, rank_results))
        print('Precision metrics: %f\nRecall metrics: %f' % calc_precision_recall(self.true_preds, rank_results))
        return rank_results


class AdditionalInput(keras.layers.Layer):
    def call(self, inputs, **kwargs):
        return inputs[0]
