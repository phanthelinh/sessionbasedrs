import matplotlib.pyplot as plt


x_arr = [0, 1, 2]
x_tick = ['Tập A', 'Tập B', 'Tập C']
ss_mrr = [0.555556, 0.494118,0.476415]
cb_mrr = [0.684211,0.717949,0.821429]
plt.plot(ss_mrr, label='Session-based', linestyle='--')
plt.plot(cb_mrr, label='Content-based')
plt.legend()
plt.xticks(x_arr, x_tick)
plt.ylabel('Giá trị')
plt.xlabel('Tập dữ liệu')
plt.title('Kết quả độ đo Recall')
plt.show()
