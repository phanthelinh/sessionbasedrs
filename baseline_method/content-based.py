import sys
from acr.util import save_to_pickle, parse_pickle_to_object
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from pathlib import Path
import os
import argparse
from acr.preprocessing.acr_preprocessing import load_csv_dataframe
from acr.tokenization import tokenize_article_to_list
from nar.preprocessing.nar_preprocess import load_sessions_hours, process_sessions_hour
import tensorflow as tf
from nar.evaluation import calc_accuracy, calc_precision_recall

NUMBER_OF_RECOMMEND = 10


def create_args_parser():
    _parser = argparse.ArgumentParser()

    _parser.add_argument(
            '--input_path', default='',
            help='Input CSV.')

    _parser.add_argument(
            '--output_matrix_path', default='')

    return _parser


parser = create_args_parser()
args = parser.parse_args()


def get_recommend_article(cleaned_articles, tfidf_matrix, articles_tfidf_matrix, list_user_read_article_ids):
    lists_recommend_id = []
    for article_id in list_user_read_article_ids:
        user_article = cleaned_articles[article_id]
        user_article_tfidf_vector = tfidf_matrix.transform([user_article])
        similarity_score = cosine_similarity(articles_tfidf_matrix, user_article_tfidf_vector)
        recommend_article_ids = similarity_score.flatten().argsort()[::-1]
        filtered_recommend = [article_id for article_id in recommend_article_ids
                              if article_id not in list_user_read_article_ids][:NUMBER_OF_RECOMMEND]
        lists_recommend_id.append(filtered_recommend)
    return lists_recommend_id


def get_idx_newsid(id, df):
    t = df.index[df['newsId'] == id]
    if len(t) != 0:
        return t[0]
    else:
        return None


def create_training_set(dataset_sessions, corpus_articles):
    sys.stdout.write('\rCreating training set...')

    train_set, label_set = [], []
    for i, data in dataset_sessions.iterrows():
        articles = data['click_article_id']
        train_id = get_idx_newsid(articles[-2], corpus_articles)

        label_id = get_idx_newsid(articles[-1], corpus_articles)
        if train_id is None or label_id is None:
            continue
        train_set.append(train_id)
        label_set.append(label_id)
    sys.stdout.write('\rCreated %d train samples.\n' % len(train_set))
    sys.stdout.flush()
    return train_set, label_set


def find_pos_label(label, rank_list):
    if label in rank_list:
        return rank_list.index(label)
    else:
        return 0


def calc_mrr(len_test_set, rank_list):
    sum = 0
    for i in range(len(rank_list)):
        if rank_list[i] != 0:
            sum += 1/rank_list[i]
    return (1/len_test_set) * sum


def main():
    print('Loading genk articles')
    dir_name = os.path.dirname(__file__)
    root_path = Path(dir_name).parent
    input_path = os.path.join(root_path, 'data/genk_data/genk.csv')
    # input_path = args.input_path
    news_articles = load_csv_dataframe(input_path)

    print('preprocessing data')
    # fullText_articles = news_articles['fullText'].tolist()
    # cleaned_articles = list(map(tokenize_article_to_list, fullText_articles))
    cleaned_articles = parse_pickle_to_object(os.path
                                              .join(root_path, "data/pickles/fullText_contentbased_preprocess.pickle"))

    print('caculating tfidf matrix')
    tfidf_matrix = parse_pickle_to_object(os.path.join(root_path, "data/saved_model/vectorize.pickle"))
    # tfidf_matrix = TfidfVectorizer(stop_words=None, min_df=2)
    article_tfidf_matrix = tfidf_matrix.fit_transform(cleaned_articles)

    sessions_dataset = []
    for (hour_index, sessions_hour) in load_sessions_hours(os.path.join(root_path, 'data/hour/session_hour=[0-7]')):
        print(hour_index)
        sessions_dataset.extend(sessions_hour)
    sessions_df = process_sessions_hour(sessions_dataset)
    with tf.compat.v1.variable_scope('create_sets'):
        x_train, y_train = create_training_set(sessions_df, news_articles)
        count_training_set = len(y_train)
        sys.stdout.write('\rProcessed %d items.' % count_training_set)
        sys.stdout.flush()

    list_recommend_ids = get_recommend_article(cleaned_articles, tfidf_matrix, article_tfidf_matrix, x_train)
    # label_pos = []
    # for idx, val in enumerate(list_recommend_ids):
    #     label_pos.append(find_pos_label(y_train[idx], val))

    # mrr = calc_mrr(len(y_train), label_pos)
    # accuracy = calc_accuracy(y_train, list_recommend_ids)
    precision, recall = calc_precision_recall(y_train, list_recommend_ids)
    print("precision: %f",  precision)
    print("recall: %f", recall)


if __name__ == '__main__':
    main()


