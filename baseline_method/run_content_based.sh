#!/bin/bash
DATA_DIR="/mnt/d/RS/src/sessionbasedrs" && python3 -m baseline_method.content-based --input_path ${DATA_DIR}/data/genk_data/genk.csv --output_matrix_path ${DATA_DIR}/data/saved_model/tfidf_corpus.pickle
