import os
import argparse
from pathlib import Path
import pandas as pd
import re
from ..tokenization import nan_to_str, tokenize_articles, get_word_freq
from ..util import save_to_pickle
from .word_embedding import load_word_embeddings, process_word_embedding
from ..tfrecord_management import export_dataframe_to_tfrecord
import tensorflow as tf


def create_args_parser():
    _parser = argparse.ArgumentParser()

    _parser.add_argument(
            '--input_csv', default='',
            help='Input CSV.')

    _parser.add_argument(
            '--input_word_embedding_model', default='')

    _parser.add_argument(
        '--output_cat_feature_encoder', default='')

    _parser.add_argument(
        '--output_metadata_encoded', default='')

    _parser.add_argument(
        '--output_word_embedding_matrices', default='')

    _parser.add_argument(
        '--output_tf_records', default='')

    return _parser


parser = create_args_parser()
args = parser.parse_args()


def remove_html_tag_web(raw_html):
    return re.compile(r'<[^>]+>').sub('', raw_html)


def load_csv_dataframe(path):
    df = pd.read_csv(path, encoding='utf-8')
    df['fullText'] = (df['title'].apply(nan_to_str) + ". " +
                      df['sapo'].apply(nan_to_str) + ". " +
                      df['content'].apply(nan_to_str)).apply(remove_html_tag_web)
    return df


def encode_value_from_encoder(value, encoder):
    return -1 if encoder.get(str(value)) is None else encoder.get(str(value))


def encode_list_value_from_encoder(list_value, encoder):
    return [-1 if encoder.get(element.lower()) is None else encoder.get(element.lower()) for element in list_value]


def processing_cat_features(df):
    # Encoder for newsId
    newsId = list(sorted(set(df['newsId'].apply(str))))
    key_list = [key for key in range(len(newsId))]
    newsId_encoder = dict(zip(newsId, key_list))
    df['newsId_encoded'] = (df['newsId'].apply(lambda x: encode_value_from_encoder(x, newsId_encoder)))

    # Encoder for catId
    catId = list(sorted(set(df['catId'].apply(str))))
    key_list = [key for key in range(1, len(catId) + 1)]
    catId.insert(0, 'UNK')
    key_list.insert(0, 0)
    catId_encoder = dict(zip(catId, key_list))
    df['catId_encoded'] = (df['catId'].apply(lambda x: encode_value_from_encoder(x, catId_encoder)))

    # Encoder for tags
    # Concatenate all tags available
    tags = list(sorted(set((';'.join(df['tags'])).lower().split(';'))))
    key_list = [key for key in range(1, len(tags) + 1)]
    # Add UNK token
    tags.insert(0, 'UNK')
    key_list.insert(0, 0)
    tags_encoder = dict(zip(tags, key_list))
    df['tags'] = df['tags'].apply(lambda x: list(x.split(';')))
    df['tags_encoded'] = df['tags'].apply(lambda x: encode_list_value_from_encoder(x, tags_encoder))

    return {'newsId_encoder': newsId_encoder,
            'catId_encoder': catId_encoder,
            'tags_encoder': tags_encoder}


def make_sequence_example(row):
    context_features = {
        'newsId': tf.train.Feature(int64_list=tf.train.Int64List(value=[row['newsId_encoded']])),
        'categories': tf.train.Feature(int64_list=tf.train.Int64List(value=[row['catId_encoded']])),
        'tags': tf.train.Feature(int64_list=tf.train.Int64List(value=row['tags_encoded']))
    }

    context = tf.train.Features(feature=context_features)

    # sequence_features = {
    #     'tags': tf.train.FeatureList(feature=[tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
    #                                           for value in row['tags_encoded']])
    # }
    #
    # features_list = tf.train.FeatureLists(feature_list=sequence_features)
    #
    # return tf.train.SequenceExample(context=context, feature_lists=features_list)
    return tf.train.Example(features=context)


def main():
    # dir_name = os.path.dirname(__file__)
    # root_path = Path(Path(dir_name).parent).parent
    #
    # input_csv = os.path.join(root_path, "data/genk_data/genk.csv")
    input_csv = args.input_csv
    print("Loading CSV: {}".format(input_csv))
    df = load_csv_dataframe(input_csv)

    print("Encoding categorical features [newsId, catId, tags] to one-hot-vector...")
    cat_feature_encoder = processing_cat_features(df)
    # output_cat_feature_encoder = os.path.join(root_path, 'data/pickles/acr_cat_feature_encoder.pickle')
    output_cat_feature_encoder = args.output_cat_feature_encoder
    print("Saving categorical feature encoder to {}".format(output_cat_feature_encoder))
    save_to_pickle(cat_feature_encoder, output_cat_feature_encoder)

    # output_metadata_encoded = os.path.join(root_path, "data/metadata_encoded.csv")
    output_metadata_encoded = args.output_metadata_encoded
    print("Saving metadata encoded to {}".format(output_metadata_encoded))
    df[['newsId_encoded', 'catId_encoded', 'tags_encoded']].to_csv(output_metadata_encoded, index=False)

    print("Tokenizing articles...")
    tokenized_articles = tokenize_articles(df)

    print("Computing word frequencies...")
    word_freq = get_word_freq(tokenized_articles)

    print("Loading word2vec model...")
    # input_word_embedding_model = os.path.join(root_path, 'data/word2vec_model/model.txt')
    input_word_embedding_model = args.input_word_embedding_model
    w2v_model = load_word_embeddings(input_word_embedding_model)

    word_embedding_matrices = process_word_embedding(w2v_model, word_freq, tokenized_articles)
    print("Num of word embeddings: {}".format(len(word_embedding_matrices)))
    # output_word_embedding_matrices = os.path.join(root_path, "data/pickles/acr_articles_word_embedding_matrices.pickle")
    output_word_embedding_matrices = args.output_word_embedding_matrices
    save_to_pickle(word_embedding_matrices, output_word_embedding_matrices)
    print("Saved word embedding matrices for all articles to {}".format(output_word_embedding_matrices))

    # output_tf_records = os.path.join(root_path, 'data/articles_tfrecords/genk_articles_*.tfrecord.gz')
    output_tf_records = args.output_tf_records
    print("Saving data to TFRecords for training to: {}".format(output_tf_records))
    data_to_export = df[['newsId_encoded',
                         'catId_encoded',
                         'tags_encoded',
                         'publishDate']]
    export_dataframe_to_tfrecord(df=data_to_export, make_sequence_example_fn=make_sequence_example,
                                 output_path=output_tf_records)


if __name__ == '__main__':
    main()
