from gensim.models import KeyedVectors
import numpy as np


def load_word_embeddings(path):
    w2v_model = KeyedVectors.load_word2vec_format(path, binary=False, unicode_errors='replace')
    return w2v_model


def process_word_embedding(w2v_model, words_freq, tokenized_articles):
    print("Omitting words have low frequencies...")
    most_freq_word = set(list(map(lambda x: x[0], words_freq.most_common(1000))))
    print("Most common words from articles: {}".format(len(most_freq_word)))

    embedding_size = w2v_model.vector_size
    FIXED_MATRIX_LEN = 1000

    articles_word_embeddings = {}
    w2v_vocab = set(w2v_model.wv.index2word)

    for newsId, article in tokenized_articles.items():
        words = []
        for sentence in article:
            for word in sentence:
                if word in w2v_vocab and word in most_freq_word and len(words) < FIXED_MATRIX_LEN:
                    words.append(w2v_model[word])

        if len(words) < FIXED_MATRIX_LEN:
            num_extra = FIXED_MATRIX_LEN - len(words)
            for i in range(num_extra):
                vec = np.zeros(shape=embedding_size)
                words.append(vec)
        articles_word_embeddings[newsId] = words
    return articles_word_embeddings
