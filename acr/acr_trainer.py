import tensorflow as tf
import logging
import os
import numpy as np
from pathlib import Path
from time import time

from acr.util import parse_pickle_to_object, get_file_names
from acr.acr_dataset import get_data_set
from acr.acr_model import ACR_Model

tfv1 = tf.compat.v1
tfv1.logging.set_verbosity(tf.compat.v1.logging.INFO)

RANDOM_SEED = 42
np.random.seed(RANDOM_SEED)


def split_dict_to_list(dic):
    key_list, value_list = [], []
    for key, value in dic.items():
        key_list.append(key)
        value_list.append(value)
    return key_list, value_list


def main(unused_argv):
    dir_name = os.path.dirname(__file__)
    root_path = Path(Path(dir_name).parent)
    logging.getLogger('tensorflow').propagate = False
    try:
        tfv1.logging.info('Loading ACR preprocessing assets')
        tfv1.logging.info('Loading categories features...')
        input_cat_feature_encoder = os.path.join(root_path, 'data/pickles/acr_cat_feature_encoder.pickle')
        cat_feature_encoder = parse_pickle_to_object(input_cat_feature_encoder)

        tfv1.logging.info('Loading word embedding matrices...')
        input_word_embedding_matrices = os.path.join(root_path, 'data/pickles/acr_articles_word_embedding_matrices.pickle')
        word_embedding_matrices = parse_pickle_to_object(input_word_embedding_matrices)

        tfv1.logging.info('Getting TFRecord data set...')
        input_tfrecord_dataset = os.path.join(root_path, 'data/articles_tfrecords')
        ds = get_data_set(get_file_names(input_tfrecord_dataset))

        tfv1.logging.info('Starting training...')

        model_output_dir = os.path.join(root_path, "data/saved_model/save_model.json")
        weights_output_dir = os.path.join(root_path, "data/saved_model/save_weights.h5")
        article_content_embedding_dir = os.path.join(root_path, 'data/pickles/article_content_embeddings.pickle')
        params = {'word_embedding_matrices': word_embedding_matrices,
                  'encoders': cat_feature_encoder,
                  'dataset': ds,
                  'model_output_dir': model_output_dir,
                  'weights_output_dir': weights_output_dir,
                  'article_content_embedding_dir': article_content_embedding_dir,
                  'mode': 'train'}
        acr_model = ACR_Model(params)

    except Exception as ex:
        tfv1.logging.error('ERROR: {}'.format(ex))
        raise


if __name__ == '__main__':
    tf.compat.v1.app.run()
