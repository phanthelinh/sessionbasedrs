from underthesea import sent_tokenize, word_tokenize
import re
from nltk.probability import FreqDist


punctuations = re.escape('!"#%&\'()*+,./:;<=>?@[\\]^-_`{|}~')

re_remove_brackets = re.compile(r'\{.*\}')
re_remove_html = re.compile(r'<(\/|\\)?.+?>', re.UNICODE)
re_transform_numbers = re.compile(r'\d', re.UNICODE)
re_transform_emails = re.compile(r'[^\s]+@[^\s]+', re.UNICODE)
re_transform_url = re.compile(r'(http|https)://[^\s]+', re.UNICODE)
# Different quotes are used.
re_quotes_1 = re.compile(r"(?u)(^|\W)[‘’′`']", re.UNICODE)
re_quotes_2 = re.compile(r"(?u)[‘’`′'](\W|$)", re.UNICODE)
re_quotes_3 = re.compile(r'(?u)[‘’`′“”]', re.UNICODE)
re_dots = re.compile(r'(?<!\.)\.\.(?!\.)', re.UNICODE)
re_punctuation = re.compile(r'([,";:]){1,2},', re.UNICODE)
re_hiphen = re.compile(r' -(?=[^\W\d_])', re.UNICODE)
re_tree_dots = re.compile(u'…', re.UNICODE)
# Differents punctuation patterns are used.
re_punkts = re.compile(r'(\w+)([%s])([ %s])' %
                       (punctuations, punctuations), re.UNICODE)
re_punkts_b = re.compile(r'([ %s])([%s])(\w+)' %
                         (punctuations, punctuations), re.UNICODE)
re_punkts_c = re.compile(r'(\w+)([%s])$' % punctuations, re.UNICODE)
re_changehyphen = re.compile(u'–')
re_doublequotes_1 = re.compile(r'(\"\")')
re_doublequotes_2 = re.compile(r'(\'\')')
re_trim = re.compile(r' +', re.UNICODE)


def clean_str(string):
    string = string.replace('\n', ' ')
    """Apply all regex above to a given string."""
    string = string.lower()
    string = re_tree_dots.sub('...', string)
    string = re.sub(r'\.\.\.', '', string)
    string = re_remove_brackets.sub('', string)
    string = re_changehyphen.sub('-', string)
    string = re_remove_html.sub(' ', string)
    string = re_transform_numbers.sub('0', string)
    string = re_transform_url.sub('URL', string)
    string = re_transform_emails.sub('EMAIL', string)
    string = re_quotes_1.sub(r'\1"', string)
    string = re_quotes_2.sub(r'"\1', string)
    string = re_quotes_3.sub('"', string)
    string = re.sub(r'[-()\"#/@;:<>{}`+=~|.!?,&]', '', string)
    string = re_dots.sub('.', string)
    string = re_punctuation.sub(r'\1', string)
    string = re_hiphen.sub(' - ', string)
    # string = re_punkts.sub(r' ', string)
    # string = re_punkts_b.sub(r' ', string)
    # string = re_punkts_c.sub(r' ', string)
    string = re_doublequotes_1.sub('\"', string)
    string = re_doublequotes_2.sub('\'', string)
    string = re_trim.sub(' ', string)
    string = re.sub(punctuations, ' ', string)
    string = re.sub('nbsp', ' ', string)

    return string.strip()


def nan_to_str(value):
    return '' if type(value) == float else value


def tokenize_articles(df):
    tokenized_articles = {}
    for index, row in df.iterrows():
        fulltext = row['fullText']
        articles_words = []
        for sent in sent_tokenize(fulltext):
            clear_sent = clean_str(sent)
            articles_words.append(word_tokenize(clear_sent))
        tokenized_articles[row['newsId']] = articles_words
    return tokenized_articles


def tokenize_article_to_list(article):
    article = clean_str(article)
    tokens = word_tokenize(article)
    cleaned_article = ' '.join([t for t in tokens])
    return cleaned_article


def get_word_freq(tokenized_articles):
    words_freq = FreqDist([word for article_id, articles in tokenized_articles.items() for sent in articles for word in sent])
    return words_freq
