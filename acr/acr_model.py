import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.layers import Dense, Conv1D, MaxPooling1D, Concatenate, Input
from keras.models import Model

from acr.util import encode_one_hot, load_model_from_file, save_model_to_file, save_to_pickle

tfv1 = tf.compat.v1
ARTICLE_CONTENT_EMBEDDINGS_DIM = 250
MATRIX_LENGTH = 1000
EMBEDDING_SIZE = 100
EPOCHS = 15
BATCH_SIZE = 10


def reverse_matrix(m):
    cols, rows = len((m[0])), len(m)
    new_m = []
    for c in range(cols):
        vec = []
        for r in range(rows):
            vec.append(m[r][c])
        new_m.append(vec)
    return np.array(new_m)


def look_up_tags_value(id_look_up, encoder, look_up_list):
    id_encoded = encoder['newsId_encoder'].get(str(id_look_up))
    rs = []
    for data in look_up_list:
        if data['newsId'].numpy() == id_encoded:
            rs = data['tags'].values.numpy()
    return rs


def look_up_label_value(id_look_up, encoder, look_up_list):
    id_encoded = encoder['newsId_encoder'].get(str(id_look_up))
    for data in look_up_list:
        if data['newsId'].numpy() == id_encoded:
            return data['categories'].numpy()


def calc_norm(obj):
    sum_ele = 0
    matrix = obj[0]
    u = matrix.shape
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            sum_ele += np.square(matrix[i][j])
    return np.sqrt(sum_ele)


def calc_cross_entropy_log_loss(label_vector, prediction):
    N = label_vector.shape[1]
    sum_val = 0
    labels_class = label_vector.numpy()[0].tolist()
    preds = K.eval(prediction)[0].tolist()
    for i in range(len(preds)):
        sum_val += labels_class[i] * np.log(preds[i])
    first_cluster = ((-1) * sum_val)
    return first_cluster


class ACR_Model:
    def __init__(self, params):
        self.encoders = params['encoders']
        self.dataset = params['dataset']
        self.label_dim = len(self.encoders['catId_encoder'])
        self.encoded_data = []
        self.weights = {}
        self.metadata_max_length = len(self.encoders['tags_encoder'])
        self.output_model_file = params['model_output_dir']
        self.output_weights_file = params['weights_output_dir']
        self.article_content_embedding_dir = params['article_content_embedding_dir']
        self.mode = params['mode']
        tf.device('/cpu:0')

        with tfv1.variable_scope("main"):
            with tfv1.variable_scope("retrieve_data_from_tfr"):
                for data in self.dataset:
                    self.encoded_data.append(data)

            with tfv1.variable_scope("word_embeddings"):
                self.word_embedding_matrices = {}
                self.newsId_list = []
                for key, item in params['word_embedding_matrices'].items():
                    self.word_embedding_matrices[key] = np.array(item)
                    self.newsId_list.append(key)
            if self.mode == 'train':
                self.build_graph_metadata_classification(self.word_embedding_matrices)
            else:
                self.build_graph_prediction()

    def build_graph_metadata_classification(self, list_of_word_embeddings):
        encoder = self.encoders
        labels = []
        matrices = []
        tags = []
        for newsId, word in list_of_word_embeddings.items():
            label = look_up_label_value(newsId, encoder, self.encoded_data)
            label_one_hot = encode_one_hot(label, encoder['catId_encoder'])
            labels.append(label_one_hot)
            matrices.append(word)
            # Get tags values
            tags_val = look_up_tags_value(newsId, encoder, self.encoded_data)
            tags.append(encode_one_hot(tags_val, encoder['tags_encoder']))

        labels = np.array(labels)
        matrices = np.array(matrices)
        tags = np.array(tags)
        self.cnn_feature_extractor(matrices, labels, tags)

    def cnn_feature_extractor(self, matrices, labels, metadata):
        input_matrix = Input(shape=(MATRIX_LENGTH, EMBEDDING_SIZE), name='input_matrix')
        conv3 = Conv1D(128, 3, activation='relu', name='conv_3')(input_matrix)
        conv4 = Conv1D(128, 4, activation='relu', name='conv_4')(input_matrix)
        conv5 = Conv1D(128, 5, activation='relu', name='conv_5')(input_matrix)

        max_pooling_3 = MaxPooling1D(pool_size=998, name='max_pooling_conv3')(conv3)
        max_pooling_4 = MaxPooling1D(pool_size=997, name='max_pooling_conv4')(conv4)
        max_pooling_5 = MaxPooling1D(pool_size=996, name='max_pooling_conv5')(conv5)

        concat_layer = Concatenate(name='concate_3_maxpool_meta')([max_pooling_3, max_pooling_4, max_pooling_5])
        first_m = Model(inputs=input_matrix, outputs=concat_layer)

        input_metadata = Input(shape=(1, self.metadata_max_length), name='input_metadata')
        metadata_dense = Dense(units=128, activation='relu', name='metadata_dense')(input_metadata)
        second_m = Model(inputs=input_metadata, outputs=metadata_dense)

        combined = Concatenate(name='concat_first_second_model')([first_m.output, second_m.output])

        fc_ace = Dense(ARTICLE_CONTENT_EMBEDDINGS_DIM, activation='relu', name='article_content_embedding')(combined)
        fc_output = Dense(self.label_dim, activation='softmax', name='metadata_prediction')(fc_ace)
        model = Model(inputs=[first_m.input, second_m.input], outputs=fc_output)
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        print(model.summary())

        model_input = [matrices, metadata]
        model.fit(model_input, labels, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=0)

        loss, accuracy = model.evaluate(model_input, labels, verbose=0)
        print('Accuracy: %f' % (accuracy * 100))
        print('Saving model to: {} and weights to: {}'.format(self.output_model_file, self.output_weights_file))
        save_model_to_file(self.output_model_file, self.output_weights_file, model)

        print('Saving Articles Content Embedding to: {}'.format(self.article_content_embedding_dir))
        ace_layer = Model(inputs=model.input, outputs=model.get_layer('article_content_embedding').output)
        ace_results = ace_layer.predict(model_input).tolist()
        ace_results = dict(zip(self.newsId_list, ace_results))
        save_to_pickle(ace_results, self.article_content_embedding_dir)

        print('Done training')

    def build_graph_prediction(self):
        model = load_model_from_file(self.output_model_file, self.output_weights_file)
        ace_layer_model = Model(inputs=model.input, outputs=model.get_layer('article_content_embedding').output)
        print(model.summary())
        pass
