import tensorflow as tf
from acr.util import merge_two_dicts


def parse_sequence_example(example):
    context_features = {
        'newsId': tf.io.FixedLenFeature([], dtype=tf.int64),
        'categories': tf.io.FixedLenFeature([], dtype=tf.int64),
        'tags': tf.io.VarLenFeature(dtype=tf.int64)
    }

    # sequence_features = {'tags': tf.io.FixedLenSequenceFeature(shape=[], dtype=tf.int64)}
    #
    # context_example, sequence_example = tf.io.parse_single_sequence_example(example,
    #                                                                         context_features=context_features,
    #                                                                         sequence_features=sequence_features)
    # merged_example = merge_two_dicts(context_example, sequence_example)
    # return merged_example
    example = tf.io.parse_single_example(serialized=example, features=context_features)
    return example


def get_data_set(file_names):
    ds = tf.data.TFRecordDataset(file_names, compression_type='GZIP')
    ds = ds.map(lambda x: parse_sequence_example(x))
    # iterator = tf.compat.v1.data.make_one_shot_iterator(ds)
    # next_ele = iterator.get_next()
    # for data in next_ele.take(5):
    #     print(data)
    return ds.take(-1)
