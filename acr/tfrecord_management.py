from acr.util import chunks
import tensorflow as tf
import sys


def save_chunk_to_tfrecords(df_chunk, make_sequence_example_fn, file_name):
    tf_options = tf.io.TFRecordOptions(compression_type='GZIP')
    tf_writer = tf.io.TFRecordWriter(file_name, tf_options)
    try:
        for index, row in df_chunk.iterrows():
            exmple = make_sequence_example_fn(row)
            tf_writer.write(exmple.SerializeToString())
    finally:
        tf_writer.close()
        sys.stdout.flush()


def export_dataframe_to_tfrecord(df, make_sequence_example_fn, output_path, num_of_files_in_tfrecord=10):
    export_file_template = output_path.replace('*', '{0:04d}')

    for chunk_index, df_chunk in enumerate(chunks(df, num_of_files_in_tfrecord)):
        print("Exporting  chunk {} (length: {})".format(chunk_index, len(df_chunk)))
        save_chunk_to_tfrecords(df_chunk, make_sequence_example_fn, export_file_template.format(chunk_index))
