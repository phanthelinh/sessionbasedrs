from typing import Optional, Any

import tensorflow as tf
import pickle
import numpy as np
import os
from keras.models import model_from_json, Model


def save_to_pickle(obj, filename):
    with tf.io.gfile.GFile(filename, 'wb') as handle:
        pickle.dump(obj, handle)


def parse_pickle_to_object(filename):
    with tf.io.gfile.GFile(filename, 'rb') as handle:
        return pickle.load(handle)


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


def encode_one_hot(values, encoder):
    values_temp = np.array(values)
    if values_temp.size > 1:
        vec = np.zeros((len(encoder),))
        for value in values:
            vec[value] = 1
        return tf.expand_dims(tf.constant(vec, dtype=tf.float32), 0)
    else:
        vec = np.zeros((len(encoder),))
        vec[values] = 1
        return tf.expand_dims(tf.constant(vec, dtype=tf.float32), 0)


def merge_two_dicts(x, y):
    return {**x, **y}


def get_file_names(path):
    """Return list of files given a regex"""
    file_names = os.listdir(path)
    return list(sorted(list(os.path.join(path, file_name) for file_name in file_names)))


def save_model_to_file(model_name, weight_name, model):
    model_json = model.to_json()
    with open(model_name, 'w') as json_file:
        json_file.write(model_json)
    # Serialize weights to HDF5
    model.save_weights(weight_name)


def load_model_from_file(model_name, weight_name):
    with open(model_name, 'r') as json_file:
        loaded_model_json = json_file.read()
    loaded_model = model_from_json(loaded_model_json)
    # Load weights
    loaded_model.load_weights(weight_name)
    loaded_model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return loaded_model
